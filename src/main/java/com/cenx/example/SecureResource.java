package com.cenx.example;

import java.security.Principal;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.annotation.security.RolesAllowed;

import org.jboss.logging.Logger;

/**
 * A JAX-RS resource for exposing secure REST endpoint.
 */
@Path("/")
public class SecureResource {

    private static Logger log = Logger.getLogger(SecureResource.class.getName());
    
    @GET
    @Path("/example")
    @Produces({ "application/json" })
    @RolesAllowed({"user", "admin"})
    public String getExample(@Context SecurityContext context) {

        Principal principal = null;

        if (context != null)
            principal = context.getUserPrincipal();

        if (principal == null)
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);

        log.info("username: " + principal.getName());
        log.info("is user in admin role: " + context.isUserInRole("admin"));
        
        return "{\"username\":\"" + principal.getName() + "\"}";

    }
    
}
