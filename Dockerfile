FROM maven as maven

WORKDIR /usr/src/app

COPY pom.xml /usr/src/app
RUN mvn dependency:resolve

COPY src /usr/src/app/src
RUN mvn package
RUN ls target

FROM jboss/wildfly

RUN /opt/jboss/wildfly/bin/add-user.sh admin admin --silent
RUN /opt/jboss/wildfly/bin/add-user.sh -a -u 'admin' -p 'tester' -g 'admini,user'
RUN /opt/jboss/wildfly/bin/add-user.sh -a -u 'ikent' -p 'tester' -g 'user'
RUN /opt/jboss/wildfly/bin/add-user.sh -a -u 'guest' -p 'tester' -g 'guest'

COPY --from=maven /usr/src/app/target/secureapp-1.0.0-SNAPSHOT.war /opt/jboss/wildfly/standalone/deployments/
COPY standalone.xml /opt/jboss/wildfly/standalone/configuration/standalone.xml

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
