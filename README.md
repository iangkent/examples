This repo contains example applications the run within WildFly Application Server.
Contains a project that is a  RESTful Web Service that is secured via wildfly security domain within the wildfly security subsystem.
The application uses standard servlet descriptor (web.xml) constraints to enable authentication and authorization.
You can see the app uses JAX-RS standard annotation to inject SecurityContext to get access to user name and role.

The security domain is using simple file based identity provider for simplicity.
I will update to used OpenLDAP container tomorrow but the code will not change.

Here are instructions to build, run and text example.

    > git clone git@bitbucket.org:iangkent/examples.git
    > cd examples

    > docker-compose up --build -d
    > docker-compose logs —follow

    > curl -u 'ikent:tester' -H "Accept: application/xml" -X GET http://localhost:8080/api/example
    {"username":"ikent”}

    > curl -u 'admin:tester' -H "Accept: application/json" -X GET http://localhost:8080/api/example
    {"username":"admin”}

    > curl -u 'guest:tester' -H "Accept: application/json" -X GET http://localhost:8080/api/example
    <html><head><title>Error</title></head><body>Forbidden</body></html>

    > curl -u 'dummy:tester' -H "Accept: application/json" -X GET http://localhost:8080/api/example
    <html><head><title>Error</title></head><body>Unauthorized</body></html>

    > docker-compose down



